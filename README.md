# AidanVanderbeck.com

## Getting Started

- `git config git-ftp.url ftp.example.com; git config git-ftp.user john@example.com; git config git-ftp.password secr3t; git config git-ftp.syncroot site`
- Make a change
- `. ./scripts/commit-ftp.sh "commit message"`

## Resources

- [Git-FTP](https://git-ftp.github.io)
- [Docs | Git-FTP](https://github.com/git-ftp/git-ftp/blob/master/man/git-ftp.1.md)
